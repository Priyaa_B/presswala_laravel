<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Presswala')</title>
    @include('includes.head')
    //<script src="{{ asset('/js/app.js').'?'.CSS_JS_VERSION }}"></script>
    {{-- <script src="{{ secure_asset('/js/app.js') }}" ></script>--}}

</head>
<!--begin::Body-->
<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
    <!--begin::Main-->
    <!--begin::Header Mobile-->
    <div id="kt_header_mobile" class="header-mobile align-items-center header-mobile-fixed">
        @include('includes.sidebar')
        @include('includes.header')
	<!--begin::Subheader-->
						<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
							<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
								<!--begin::Info-->
								<div class="d-flex align-items-center flex-wrap mr-2">
									<!--begin::Page Title-->
									<h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">@yield('Subtitle','Presswala')</h5>
									<!--end::Page Title-->
									<!--begin::Actions-->
									<div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
									<span class="text-muted font-weight-bold mr-4">#XRS-45670</span>
									<a href="#" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a>
									<!--end::Actions-->
								</div>
								<!--end::Info-->
							
							</div>
						</div>
						<!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">

                @yield('content')

            </div>
        </div>

        @include('includes.footer')


	</div>
		<!--end::Main-->

    @include('includes.foot')

    @stack('view-scripts')
	</body>
	<!--end::Body-->
</html>

