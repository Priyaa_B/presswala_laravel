<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
{{--<title>Portal</title>--}}
<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />

<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="user-id" content="{{Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : ''}}">
<meta name="unread" content="{{url('/unread-notification')}}">
<!--begin::Page Vendors Styles(used by this page)-->
		<link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<link href="assets/css/themes/layout/header/base/light.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/themes/layout/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/themes/layout/brand/dark.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/themes/layout/aside/dark.css" rel="stylesheet" type="text/css" />
		<!--end::Layout Themes-->
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />

{{-- <link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" type="text/css">

<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.css')}}">

<link rel="stylesheet" href="{{ asset('assets/css/custom.css').'?'.CSS_JS_VERSION}}" type="text/css">
<!-- Bootstrap Tagsinput -->
<link rel="stylesheet" href="{{ asset('assets/bootstrap-tagsinput.css').'?'.CSS_JS_VERSION}}" type="text/css"> --}}


<script>
	//Reload when Returning From backward button - Chrome
    if (!!window.performance && window.performance.navigation.type === 2) {
        window.location.reload(); // reload whole page
    }
    WebFont.load({
        google: {"families":["Lato:300,400,700,900"]},
        custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['/assets/css/fonts.min.css']},
        active: function() {
            sessionStorage.fonts = true;
        }
    });
</script>
{{-- <script src="{{asset('assets/js/plugin/ckeditor/ckeditor.js')}}" type="text/javascript"></script> --}}
<script src="{{asset('assets/js/core/jquery.3.2.1.min.js')}}"></script>

<link rel="stylesheet" href="{{asset('dropzone/dropzone.min.css')}}">
<style>
    .dropzone{
        border: 2px dashed #5c55bf !important;
    }
</style>
@stack('view-css')
