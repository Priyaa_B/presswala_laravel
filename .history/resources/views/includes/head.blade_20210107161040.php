<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
{{--<title>Portal</title>--}}
<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />

<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="user-id" content="{{Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : ''}}">
<meta name="unread" content="{{url('/unread-notification')}}">
<link rel="icon" href="{{asset('assets/img/icon.ico')}}" type="image/x-icon"/>

<!-- Fonts and icons -->
<script src="{{asset('assets/js/plugin/webfont/webfont.min.js')}}"></script>
<!-- CSS Files -->
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/atlantis.min.css')}}">

<link rel="stylesheet" href="{{asset('assets/css/fixedColumns.bootstrap4.min.css')}}">


<link rel="stylesheet" href="{{ asset('assets/css/pretty-checkbox.css') }}" type="text/css">

<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" type="text/css">

<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.css')}}">

<link rel="stylesheet" href="{{ asset('assets/css/custom.css').'?'.CSS_JS_VERSION}}" type="text/css">
<!-- Bootstrap Tagsinput -->
<link rel="stylesheet" href="{{ asset('assets/bootstrap-tagsinput.css').'?'.CSS_JS_VERSION}}" type="text/css">

{{--<link rel="stylesheet" href="{{asset('assets/css/font.min.css')}}">--}}

<!-- CSS Just for demo purpose, don't include it in your project -->
{{--<link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">--}}
<script>
	//Reload when Returning From backward button - Chrome
    if (!!window.performance && window.performance.navigation.type === 2) {
        window.location.reload(); // reload whole page
    }
    WebFont.load({
        google: {"families":["Lato:300,400,700,900"]},
        custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['/assets/css/fonts.min.css']},
        active: function() {
            sessionStorage.fonts = true;
        }
    });
</script>
<script src="{{asset('assets/js/plugin/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/core/jquery.3.2.1.min.js')}}"></script>

<link rel="stylesheet" href="{{asset('dropzone/dropzone.min.css')}}">
<style>
    .dropzone{
        border: 2px dashed #5c55bf !important;
    }
</style>
@stack('view-css')
