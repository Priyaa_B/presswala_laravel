<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Port')</title>
    @include('includes.head')
    //<script src="{{ asset('/js/app.js').'?'.CSS_JS_VERSION }}" ></script>
{{--    <script src="{{ secure_asset('/js/app.js') }}" ></script>--}}

</head>
<body cz-shortcut-listen="true" data-background-color="bg3">
    <div class="wrapper">
        @include('includes.header')
        @include('includes.sidebar')

        <div class="main-panel">
            <div class="content">

                @yield('content')

            </div>
        </div>

        @include('includes.footer')

    </div>

    @include('includes.foot')

    @stack('view-scripts')
</body>
</html>
