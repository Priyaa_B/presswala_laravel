<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Presswala')</title>
    @include('includes.head')
    //<script src="{{ asset('/js/app.js').'?'.CSS_JS_VERSION }}" ></script>
{{--    <script src="{{ secure_asset('/js/app.js') }}" ></script>--}}

</head>
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
		<!--begin::Main-->
		<!--begin::Header Mobile-->
		<div id="kt_header_mobile" class="header-mobile align-items-center header-mobile-fixed">
		    @include('includes.sidebar')
		    @include('includes.header')

		    <!--begin::Entry-->
		    <div class="d-flex flex-column-fluid">
		        <!--begin::Container-->
		        <div class="container">

		            @yield('content')

		        </div>
		    </div>

		    @include('includes.footer')


    </div>

    @include('includes.foot')

    @stack('view-scripts')
</body>
</html>
